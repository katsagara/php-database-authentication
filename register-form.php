<?php 
    if(@$_GET['Empty']==true)
    {
?>
    <div class="alert-light text-danger text-center py-3"><?php echo $_GET['Empty'] ?></div>                                
<?php } ?>
<?php 
    if(@$_GET['Invalid']==true)
    {
?>
    <div class="alert-light text-danger text-center py-3"><?php echo $_GET['Invalid'] ?></div>                                
<?php } ?>  
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/bootstrap.css">
    <title>Registration Form in PHP With Session</title>
</head>
<body style="background:#CCC;">       
       <div class="container">
            <div class="row">
                <div class="col-lg-6 m-auto">
                    <div class="card bg-dark mt-5">
                        <div class="card-title bg-primary text-white mt-5">
                            <h3 class="text-center py-3">Registration Form in PHP </h3>
                        </div>
                        <div class="card-body">
                            <form method="post" action="registration.php">
                               <?php // include('errors.php'); ?>
                                <div class="input-group">
                                    <input type="text" name="UName" placeholder=" Password" value="<?php echo $username; ?>">
                                </div>
                                <div class="input-group">
                                    <input type="password" name="Pass_1" placeholder=" Password">
                                </div>
                                <div class="input-group">
                                    <input type="password" name="Pass_2" placeholder=" Confirm password">
                                </div>
                                <div class="input-group">
                                    <button type="submit" class="btn" name="Registration">Register</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div><?php echo '<a href="show-users.php">Fetch all users</a>'; ?></div>
    </body>
</html>